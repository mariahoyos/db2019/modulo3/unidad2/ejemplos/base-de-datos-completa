<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plantas".
 *
 * @property int $numeroplanta
 * @property double $precio
 *
 * @property Plazas[] $plazas
 */
class Plantas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plantas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numeroplanta'], 'required'],
            [['numeroplanta'], 'integer'],
            [['precio'], 'number'],
            [['numeroplanta'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numeroplanta' => 'Numeroplanta',
            'precio' => 'Precio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlazas()
    {
        return $this->hasMany(Plazas::className(), ['numeroplanta' => 'numeroplanta']);
    }
}
