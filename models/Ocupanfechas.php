<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ocupanfechas".
 *
 * @property int $idocupanfechas
 * @property int $idocupan
 * @property string $fechaentrada
 * @property string $fechasalida
 *
 * @property Ocupan $ocupan
 */
class Ocupanfechas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ocupanfechas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idocupan'], 'integer'],
            [['fechaentrada', 'fechasalida'], 'safe'],
            [['idocupan', 'fechaentrada'], 'unique', 'targetAttribute' => ['idocupan', 'fechaentrada']],
            [['idocupan'], 'exist', 'skipOnError' => true, 'targetClass' => Ocupan::className(), 'targetAttribute' => ['idocupan' => 'idocupan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idocupanfechas' => 'Idocupanfechas',
            'idocupan' => 'Idocupan',
            'fechaentrada' => 'Fechaentrada',
            'fechasalida' => 'Fechasalida',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOcupan()
    {
        return $this->hasOne(Ocupan::className(), ['idocupan' => 'idocupan']);
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->fechaentrada = Yii::$app->formatter->asDate($this->fechaentrada, 'php:d-m-Y h:i:s');
        $this->fechasalida = Yii::$app->formatter->asDate($this->fechasalida, 'php:d-m-Y h:i:s');
    
        
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->fechaentrada = Yii::$app->formatter->asDate($this->fechaentrada, 'php:Y-m-d h:i:s');
        $this->fechasalida = Yii::$app->formatter->asDate($this->fechasalida, 'php:Y-m-d h:i:s');
        return true;
    }
}
