<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ocupan".
 *
 * @property int $idocupan
 * @property string $matricula
 * @property int $idplaza
 *
 * @property Plazas $plaza
 * @property Vehiculos $matricula0
 * @property Ocupanfechas[] $ocupanfechas
 */
class Ocupan extends \yii\db\ActiveRecord
{
    public $fechaentrada;
    public $fechasalida;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ocupan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idplaza'], 'integer'],
            [['matricula'], 'string', 'max' => 10],
            [['matricula', 'idplaza'], 'unique', 'targetAttribute' => ['matricula', 'idplaza']],
            [['idplaza'], 'exist', 'skipOnError' => true, 'targetClass' => Plazas::className(), 'targetAttribute' => ['idplaza' => 'idplaza']],
            [['matricula'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculos::className(), 'targetAttribute' => ['matricula' => 'matricula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idocupan' => 'Idocupan',
            'matricula' => 'Matricula',
            'idplaza' => 'Idplaza',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaza()
    {
        return $this->hasOne(Plazas::className(), ['idplaza' => 'idplaza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatricula0()
    {
        return $this->hasOne(Vehiculos::className(), ['matricula' => 'matricula']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOcupanfechas()
    {
        return $this->hasMany(Ocupanfechas::className(), ['idocupan' => 'idocupan']);
    }
}
