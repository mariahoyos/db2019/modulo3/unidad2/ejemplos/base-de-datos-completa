<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vehiculos".
 *
 * @property string $matricula
 * @property string $dni
 * @property string $color
 * @property string $modelo
 *
 * @property Ocupan[] $ocupans
 * @property Plazas[] $plazas
 */
class Vehiculos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vehiculos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula'], 'required'],
            [['matricula'], 'string', 'max' => 10],
            [['dni'], 'string', 'max' => 9],
            [['color'], 'string', 'max' => 20],
            [['modelo'], 'string', 'max' => 15],
            [['matricula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'matricula' => 'Matricula',
            'dni' => 'Dni',
            'color' => 'Color',
            'modelo' => 'Modelo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOcupans()
    {
        return $this->hasMany(Ocupan::className(), ['matricula' => 'matricula']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlazas()
    {
        return $this->hasMany(Plazas::className(), ['idplaza' => 'idplaza'])->viaTable('ocupan', ['matricula' => 'matricula']);
    }
}
