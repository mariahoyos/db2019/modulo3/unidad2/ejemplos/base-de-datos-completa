<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plazas".
 *
 * @property int $idplaza
 * @property int $numeroplanta
 * @property int $numeroplaza
 * @property int $espacio
 *
 * @property Ocupan[] $ocupans
 * @property Vehiculos[] $matriculas
 * @property Plantas $numeroplanta0
 */
class Plazas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plazas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numeroplanta', 'numeroplaza', 'espacio'], 'integer'],
            [['numeroplanta', 'numeroplaza'], 'unique', 'targetAttribute' => ['numeroplanta', 'numeroplaza']],
            [['numeroplanta'], 'exist', 'skipOnError' => true, 'targetClass' => Plantas::className(), 'targetAttribute' => ['numeroplanta' => 'numeroplanta']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idplaza' => 'Idplaza',
            'numeroplanta' => 'Numeroplanta',
            'numeroplaza' => 'Numeroplaza',
            'espacio' => 'Espacio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOcupans()
    {
        return $this->hasMany(Ocupan::className(), ['idplaza' => 'idplaza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Vehiculos::className(), ['matricula' => 'matricula'])->viaTable('ocupan', ['idplaza' => 'idplaza']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumeroplanta0()
    {
        return $this->hasOne(Plantas::className(), ['numeroplanta' => 'numeroplanta']);
    }
}
