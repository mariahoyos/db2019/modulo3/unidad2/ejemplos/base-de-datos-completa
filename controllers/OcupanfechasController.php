<?php

namespace app\controllers;

use Yii;
use app\models\Ocupanfechas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OcupanFechasController implements the CRUD actions for OcupanFechas model.
 */
class OcupanfechasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OcupanFechas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ocupanfechas::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMostrar($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ocupanfechas::find()->where("idocupan=$id"),
        ]);

        return $this->render('index_1', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OcupanFechas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OcupanFechas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OcupanFechas();
                echo "<pre>";
        var_dump(Yii::$app->request->post());
        echo "</pre>";

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idocupanfechas]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /*
     * public function actionCreate1($id)
     * consulta cruzada ocupan -> ocupanfechas
     
    {
        $model = new OcupanFechas();
        $model->idocupan=$id;
                
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idocupanfechas]);
        }
        
        return $this->render('create1', [
            'model' => $model,
        ]);
    }
    */
    
    /**
     * Updates an existing OcupanFechas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idocupanfechas]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OcupanFechas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OcupanFechas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OcupanFechas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ocupanfechas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionListarfechas($idocupan){
        $dataProvider = new ActiveDataProvider([
            'query' => Ocupanfechas::find()->select('*')->where("idocupan=$idocupan"),
        ]);
        
        $plaza = \app\models\Ocupan::findOne($idocupan);
        
        return $this->render('listarfechas', [
            'dataProvider' => $dataProvider,
            'plaza'=>$plaza,
        ]);
        
    }
    
    
}
