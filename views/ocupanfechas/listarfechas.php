<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fechas de uso de la plaza';
/* memoriza la dirección de donde vienes */
$atras=Yii::$app->request->referrer;
/*modifica los breadcrumbs para volver a la página en la que estábamos anteriormente*/
$this->params['breadcrumbs'][] = ['label'=>'Plaza ocupada','url'=>$atras];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ocupan-fechas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $plaza,
        'attributes' => [
            //'idocupan',
            'matricula',
            'idplaza',
        ],
    ]) ?>
    
    
    
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idocupanfechas',
            //'idocupan',
            'fechaentrada',
            'fechasalida',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
