<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OcupanFechas */

$this->title = 'Update Ocupan Fechas: ' . $model->idocupanfechas;
$this->params['breadcrumbs'][] = ['label' => 'Ocupan Fechas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idocupanfechas, 'url' => ['view', 'id' => $model->idocupanfechas]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ocupan-fechas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
