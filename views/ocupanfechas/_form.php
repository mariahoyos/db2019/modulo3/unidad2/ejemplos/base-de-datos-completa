<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\OcupanFechas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ocupan-fechas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idocupan')->textInput() ?>
    <div class="col-sm-6">
        <label class="control-label">Hora de entrada</label>
        <?= DateTimePicker::widget([
            'model' => $model,
            'attribute'=>'fechaentrada',
            'readonly' => true,
            'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy/mm/dd hh:ii:ss',
                    'todayHighlight' => true,
                    'todayBtn' => true,
            ]
        ]); ?>
    </div>
    
    <div class="col-sm-6">
        <label class="control-label">Hora de salida</label>
        <?= DateTimePicker::widget([
            'model'=>$model,
            'attribute'=>'fechasalida',
            'readonly' => true,
            'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy/mm/dd hh:ii:ss',
                    'todayHighlight' => true,
                    'todayBtn' => true,
            ]
        ]); ?>
    </div>
            
    <div class="form-group cold-sm-2">

        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
        
    <?php ActiveForm::end(); ?>

</div>
