<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OcupanFechas */

$this->title = 'Create Ocupan Fechas';
$this->params['breadcrumbs'][] = ['label' => 'Ocupan Fechas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ocupan-fechas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
