<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ocupan Fechas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ocupan-fechas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    
        
        <?= Html::a('Create Ocupan Fechas', ['create1','id'=>$dataProvider->models[0]->idocupan], ['class' => 'btn btn-success']) ?>
            
    
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idocupanfechas',
            'idocupan',
            'fechaentrada',
            'fechasalida',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
