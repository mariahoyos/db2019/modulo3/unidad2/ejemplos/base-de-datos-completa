<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vehiculos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehiculos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Vehiculos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'matricula',
            'dni',
            'color',
            'modelo',
            [
                'label'=>'foto del coche',
                'format'=>'raw',
                'value'=>function($model){
                    return Html::img('@web/imgs/' . $model->foto,['class'=>"img-rounded",'width'=>"100px"]);
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
            'template'=>'{view}{update}{delete}{libros}',    
            'buttons'=>[
                'libros'=>function($url,$model){
                    return Html::a('<span class = "glyphicon glyphicon-list-alt"></span>',['ocupan/listar','matricula'=>$model->matricula]);
                    
                },
            ],       
            ],
        ],
    ]); ?>


</div>
