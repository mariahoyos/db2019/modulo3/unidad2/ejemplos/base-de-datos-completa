<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Plantas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plantas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numeroplanta')->textInput() ?>

    <?= $form->field($model, 'precio')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
