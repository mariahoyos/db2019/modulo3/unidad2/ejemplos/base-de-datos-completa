<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Plantas */

$this->title = 'Update Plantas: ' . $model->numeroplanta;
$this->params['breadcrumbs'][] = ['label' => 'Plantas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->numeroplanta, 'url' => ['view', 'id' => $model->numeroplanta]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="plantas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
