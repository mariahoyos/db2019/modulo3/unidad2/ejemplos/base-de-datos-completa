<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Plazas */

$this->title = 'Create Plazas';
$this->params['breadcrumbs'][] = ['label' => 'Plazas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plazas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
