<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Plazas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plazas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numeroplanta')->textInput() ?>

    <?= $form->field($model, 'numeroplaza')->textInput() ?>

    <?= $form->field($model, 'espacio')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
