<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Plazas */

$this->title = 'Update Plazas: ' . $model->idplaza;
$this->params['breadcrumbs'][] = ['label' => 'Plazas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idplaza, 'url' => ['view', 'id' => $model->idplaza]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="plazas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
