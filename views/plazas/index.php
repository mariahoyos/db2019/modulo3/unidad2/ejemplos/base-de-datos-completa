<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Plazas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plazas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Plazas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idplaza',
            'numeroplanta',
            'numeroplaza',
            'espacio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
