<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ocupan */

$this->title = 'Create Ocupan';
$this->params['breadcrumbs'][] = ['label' => 'Ocupans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ocupan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
