<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $dataProvider->models[0]->matricula0 es igual que $vehiculo

$this->title = 'Datos Vehículo ' . $vehiculo->matricula;
/* memoriza la dirección de donde vienes */
$atras=Yii::$app->request->referrer;
/*modifica los breadcrumbs para volver a la página en la que estábamos anteriormente*/
$this->params['breadcrumbs'][] = ['label'=>'Vehículos','url'=>$atras];


$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ocupan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $vehiculo,
        'attributes' => [
            'matricula',
            'dni',
            'color',
            'modelo',
        ],
    ]) ?>
    
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            'idocupan',
            //'matricula',
            //'matricula0.color',
            'idplaza',
            //'fechaentrada',
            //'fechasalida',
            //'plaza.numeroplaza',

            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{mostrar}',
            'buttons' => [
                'mostrar' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-road"></span>', ['ocupanfechas/listarfechas', 'idocupan' => $model->idocupan]); 
                    //return Html::a('<span class = "glyphicon glyphicon-list-alt"></span>',['ocupan/listar','matricula'=>$model->matricula]);
                },
	        ],
            ],
        ],
    ]); ?>


</div>
