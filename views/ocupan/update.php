<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ocupan */

$this->title = 'Update Ocupan: ' . $model->idocupan;
$this->params['breadcrumbs'][] = ['label' => 'Ocupans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idocupan, 'url' => ['view', 'id' => $model->idocupan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ocupan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
