<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ocupans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ocupan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ocupan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idocupan',
            'matricula',
            'idplaza',

            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {mostrar}',
            'buttons' => [
                'mostrar' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-road"></span>', ['ocupanfechas/mostrar', 'id' => $model->idocupan]); 
                },
	        ],
            ],
        ],
    ]); ?>


</div>
