﻿DROP DATABASE IF EXISTS parking;
CREATE DATABASE parking;
USE parking;

CREATE OR REPLACE TABLE vehiculos(
  matricula varchar(10),
  dni varchar(9),
  color varchar(20),
  modelo varchar(15),
  PRIMARY KEY (matricula)
);

CREATE OR REPLACE TABLE plantas(
  numeroplanta int,
  precio float,
  PRIMARY KEY(numeroplanta)
);

CREATE OR REPLACE TABLE plazas(
  idplaza int AUTO_INCREMENT,
  numeroplanta int,
  numeroplaza int,
  espacio int,
  PRIMARY KEY(idplaza)
);

CREATE OR REPLACE TABLE ocupan(
  idocupan int AUTO_INCREMENT,
  matricula varchar(10),
  idplaza int,
  PRIMARY KEY(idocupan)
);

CREATE OR REPLACE TABLE ocupanfechas(
  idocupanfechas int AUTO_INCREMENT,
  idocupan int,
  fechaentrada datetime,
  fechasalida datetime,
  PRIMARY KEY(idocupanfechas)
);


ALTER TABLE plazas
  ADD CONSTRAINT fk_plaza_plantas
  FOREIGN KEY(numeroplanta) REFERENCES plantas(numeroplanta),

  ADD CONSTRAINT unique_plazas_numplanta
  UNIQUE(numeroplanta,numeroplaza);

ALTER TABLE ocupan
  ADD CONSTRAINT fk_ocupan_vehiculos
  FOREIGN KEY(matricula) REFERENCES vehiculos(matricula),

  ADD CONSTRAINT fk_ocupan_plazas
  FOREIGN KEY(idplaza) REFERENCES plazas(idplaza),

  ADD CONSTRAINT unique_matricula_id_plaza
  UNIQUE(matricula,idplaza);

ALTER TABLE ocupanfechas
  ADD CONSTRAINT fk_ocupanfechas_ocupan
  FOREIGN KEY(idocupan) REFERENCES ocupan(idocupan),

  ADD CONSTRAINT unique_idocupan_fechahoraentrada
  UNIQUE(idocupan,fechaentrada);



